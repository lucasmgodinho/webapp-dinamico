
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
    <title>Tipos de Usuário</title>
</head>
<body>
    <h1>Tipos de Usuário</h1>

    <h2>Cadastro</h2>
    <form method="post" action="#{tipoUsuarioController.salvarTipoUsuario}">
        <input type="text" name="nomeTipoUsuario" placeholder="Nome do Tipo de Usuário" value="#{tipoUsuarioController.tipoUsuario.nomeTipoUsuario}" required/><br/>
        <input type="submit" value="Salvar"/>
    </form>

    <h2>Listagem</h2>
    <table border="1">
        <thead>
            <tr>
                <th>ID</th>
                <th>Nome do Tipo de Usuário</th>
                <th>Ações</th>
            </tr>
        </thead>
        <tbody>
            <c:forEach items="#{tipoUsuarioController.tiposUsuarios}" var="tipoUsuario">
                <tr>
                    <td><c:out value="#{tipoUsuario.idTipoUsuario}"/></td>
                    <td><c:out value="#{tipoUsuario.nomeTipoUsuario}"/></td>
                    <td>
                        <a href="#" onclick="editarTipoUsuario(<c:out value="#{tipoUsuario.idTipoUsuario}"/>);">Editar</a>
                        <a href="#" onclick="excluirTipoUsuario(<c:out value="#{tipoUsuario.idTipoUsuario}"/>);">Excluir</a>
                    </td>
                </tr>
            </c:forEach>
        </tbody>
    </table>

    <script>
        function editarTipoUsuario(idTipoUsuario) {
        }

        function excluirTipoUsuario(idTipoUsuario) {
        }
    </script>
</body>
</html>
