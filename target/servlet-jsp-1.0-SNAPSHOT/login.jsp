<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>TO DO Dinâmico</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <style type="text/css">
            .centro {
                margin: 0 auto;
                width: 480px;
            }
        </style>
    </head>
    <body>
        <div class="centro">
            <h1>TO DO Dinâmico</h1>
            <hr />
            <h5>Login</h5>
            <c:if test="${not empty requestScope.erro}">
                <p style="color: red">${requestScope.erro}</p>
            </c:if>
            <p>Entre com suas credenciais</p>
            <form action="/todo/login" method="post">
                <label for="username">Usuário</label> <br />
                <input id="username" type="text" name="username" placeholder="Usuário" /><br />
                <label for="username">Senha</label> <br />
                <input id="password" type="password" name="password" placeholder="Senha"/><br /> 
                <input type="submit" name="entrar" value="Entrar" /> 
            </form>
        </div>        
    </body>
</html>