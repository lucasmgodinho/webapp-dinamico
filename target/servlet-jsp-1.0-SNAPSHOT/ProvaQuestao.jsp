<%-- 
    Document   : ProvaQuestao
    Created on : 1 de jul. de 2023, 00:07:33
    Author     : lucas
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Prova e Questão</title>
    </head>
    <body>
        <h1>Prova e Questão</h1>

        <h2>Cadastro</h2>
        <form method="post" action="#{provaQuestaoController.salvarProvaQuestao}">
            <input type="number" name="prova_idProva" placeholder="ID da Prova" value="#{provaQuestaoController.provaQuestao.prova_idProva}" required/><br/>
            <input type="number" name="questao_idQuestao" placeholder="ID da Questão" value="#{provaQuestaoController.provaQuestao.questao_idQuestao}" required/><br/>
            <input type="submit" value="Salvar"/>
        </form>

        <h2>Listagem</h2>
        <table border="1">
            <thead>
                <tr>
                    <th>ID da Prova</th>
                    <th>ID da Questão</th>
                    <th>Ações</th>
                </tr>
            </thead>
            <tbody>
            <c:forEach items="#{provaQuestaoController.provaQuestoes}" var="provaQuestao">
                <tr>
                    <td><c:out value="#{provaQuestao.prova_idProva}"/></td>
                <td><c:out value="#{provaQuestao.questao_idQuestao}"/></td>
                <td>
                    <a href="#" onclick="editarProvaQuestao(<c:out value="#{provaQuestao.prova_idProva}"/>, <c:out value="#{provaQuestao.questao_idQuestao}"/>);">Editar</a>
                    <a href="#" onclick="excluirProvaQuestao(<c:out value="#{provaQuestao.prova_idProva}"/>, <c:out value="#{provaQuestao.questao_idQuestao}"/>);">Excluir</a>
                </td>
                </tr>
            </c:forEach>
        </tbody>
    </table>

        <script>
function editarProvaQuestao(prova_idProva, questao_idQuestao) {
    }
    
    function excluirProvaQuestao(prova_idProva, questao_idQuestao) {
        }
        </script>
</body>
</html>
