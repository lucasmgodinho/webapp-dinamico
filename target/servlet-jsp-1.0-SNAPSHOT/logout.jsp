<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
    <title>Logout</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <style type="text/css">
        .centro {
            margin: 0 auto;
            width: 480px;
        }
    </style>
</head>
<body>
    <div class="centro">
        <h1>Logout</h1>
        <hr />
        <h2>Logout realizado com sucesso!</h2>
        <p>Você foi desconectado de sua conta.</p>
        <p><a href="/todo/login.jsp">Fazer login novamente</a></p>
    </div>        
</body>
</html>
