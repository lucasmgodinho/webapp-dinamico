<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>TO DO Dinâmico</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <style type="text/css">
            .centro {
                margin: 0 auto;
                width: 480px;
            }
        </style>
    </head>
    <body>
        <div class="centro">
            <h1>TO DO Dinâmico</h1>
            <h2>Lista de tarefas web dinâmica </h2>
            <p>Insira suas tarefas na lista abaixo e controle o que precisa fazer.</p>
            <ul>                
                <c:forEach var="tarefa" items="${sessionScope.tarefas}"> 
                   <c:choose>
                    <c:when test="${tarefa.importante}">
                      <li><strong>${tarefa.descricao}</strong></li>      
                    </c:when>
                    <c:otherwise>
                     <li>${tarefa.descricao}</li>
                    </c:otherwise>
                  </c:choose>
                </c:forEach>
            </ul> 
            <hr />
            <form action="/todo/adicionar" method="post">
                <input type="text" name="descricao" placeholder="Insira nova tarefa" />
                 <input type="checkbox" name="importante"/> Importante
                <input type="submit" name="enviar" value="Enviar" /> 
            </form>
        </div>        
    </body>
</html>