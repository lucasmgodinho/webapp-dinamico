/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package todo.controllers;

import todo.DAO.UsuarioDAO;
import todo.model.Usuario;
import java.util.List;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

@Path("/usuarios")
public class UsuarioRestController {

    private UsuarioDAO usuarioDAO;

    public UsuarioRestController() {
        usuarioDAO = new UsuarioDAO();
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<Usuario> listarUsuarios() {
        return usuarioDAO.listarUsuarios();
    }

    @GET
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Usuario buscarUsuario(@PathParam("id") int id) {
        return usuarioDAO.buscarUsuario(id);
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Usuario adicionarUsuario(Usuario usuario) {
        return usuarioDAO.adicionarUsuario(usuario);
    }

    @PUT
    @Path("/{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Usuario atualizarUsuario(@PathParam("id") int id, Usuario usuario) {
        usuario.setIdUsuario(id);
        return usuarioDAO.atualizarUsuario(usuario);
    }

    @DELETE
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public void excluirUsuario(@PathParam("id") int id) {
        usuarioDAO.excluirUsuario(id);
    }
}
