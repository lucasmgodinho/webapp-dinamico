/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package todo.controllers;

import javax.ws.rs.*;
import javax.ws.rs.core.*;
import java.util.List;
import todo.DAO.QuestaoDAO;
import todo.model.Questao;

@Path("/questoes")
public class QuestaoRestController {

    private QuestaoDAO questaoDAO;

    public QuestaoRestController() {
        questaoDAO = new QuestaoDAO();
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<Questao> listarQuestoes() {
        return questaoDAO.listarQuestoes();
    }

    @GET
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response obterQuestaoPorId(@PathParam("id") int id) {
        Questao questao = questaoDAO.obterQuestaoPorId(id);
        if (questao != null) {
            return Response.ok(questao).build();
        } else {
            return Response.status(Response.Status.NOT_FOUND).build();
        }
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public Response adicionarQuestao(Questao questao) {
        questaoDAO.adicionarQuestao(questao);
        return Response.status(Response.Status.CREATED).build();
    }

    @PUT
    @Path("/{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response atualizarQuestao(@PathParam("id") int id, Questao questao) {
        questao.setIdQuestao(id);
        if (questaoDAO.atualizarQuestao(questao)) {
            return Response.ok().build();
        } else {
            return Response.status(Response.Status.NOT_FOUND).build();
        }
    }

    @DELETE
    @Path("/{id}")
    public Response excluirQuestao(@PathParam("id") int id) {
        if (questaoDAO.excluirQuestao(id)) {
            return Response.ok().build();
        } else {
            return Response.status(Response.Status.NOT_FOUND).build();
        }
    }
}
