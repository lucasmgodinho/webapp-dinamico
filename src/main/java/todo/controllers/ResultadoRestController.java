/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package todo.controllers;
import todo.DAO.ResultadoDAO;
import todo.model.Resultado;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

@Path("/resultados")
public class ResultadoRestController {
    private ResultadoDAO resultadoDAO;

    public ResultadoRestController() {
        resultadoDAO = new ResultadoDAO();
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<Resultado> obterResultados() {
        return resultadoDAO.listarResultados();
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public Response adicionarResultado(Resultado resultado) {
        resultadoDAO.salvar(resultado);
        return Response.status(Response.Status.CREATED).build();
    }

    @PUT
    @Path("/{usuario_idUsuario}/{prova_idProva}")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response atualizarResultado(@PathParam("usuario_idUsuario") int usuarioId,
                                       @PathParam("prova_idProva") int provaId,
                                       Resultado resultado) {
        return Response.status(Response.Status.OK).build();
    }

    @DELETE
    @Path("/{usuario_idUsuario}/{prova_idProva}")
    public Response excluirResultado(@PathParam("usuario_idUsuario") int usuarioId,
                                     @PathParam("prova_idProva") int provaId) {
        // Verificar se o resultado existe e realizar a exclusão
        // ...

        return Response.status(Response.Status.OK).build();
    }
}


