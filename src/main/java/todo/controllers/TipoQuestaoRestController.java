/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package todo.controllers;
import todo.DAO.TipoQuestaoDAO;
import todo.model.TipoQuestao;

import javax.ws.rs.*;
import javax.ws.rs.core.*;
import java.util.List;

@Path("/tipoquestao")
public class TipoQuestaoRestController {

    private TipoQuestaoDAO tipoQuestaoDAO;

    public TipoQuestaoRestController() {
        tipoQuestaoDAO = new TipoQuestaoDAO();
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response listarTiposQuestao() {
        List<TipoQuestao> tiposQuestao = tipoQuestaoDAO.listarTiposQuestao();
        return Response.ok(tiposQuestao).build();
    }

    @GET
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response buscarTipoQuestao(@PathParam("id") int id) {
        TipoQuestao tipoQuestao = tipoQuestaoDAO.buscarTipoQuestao(id);
        if (tipoQuestao != null) {
            return Response.ok(tipoQuestao).build();
        } else {
            return Response.status(Response.Status.NOT_FOUND).build();
        }
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response adicionarTipoQuestao(TipoQuestao tipoQuestao) {
        tipoQuestaoDAO.salvar(tipoQuestao);
        return Response.ok(tipoQuestao).status(Response.Status.CREATED).build();
    }

    @PUT
    @Path("/{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response atualizarTipoQuestao(@PathParam("id") int id, TipoQuestao tipoQuestaoAtualizado) {
        TipoQuestao tipoQuestao = tipoQuestaoDAO.buscarTipoQuestao(id);
        if (tipoQuestao != null) {
            tipoQuestao.setIdTipoQuestao(tipoQuestaoAtualizado.getIdTipoQuestao());
            tipoQuestao.setNomeTipoQuestao(tipoQuestaoAtualizado.getNomeTipoQuestao());
            tipoQuestaoDAO.atualizar(tipoQuestao);
            return Response.ok(tipoQuestao).build();
        } else {
            return Response.status(Response.Status.NOT_FOUND).build();
        }
    }

    @DELETE
    @Path("/{id}")
    public Response excluirTipoQuestao(@PathParam("id") int id) {
        TipoQuestao tipoQuestao = tipoQuestaoDAO.buscarTipoQuestao(id);
        if (tipoQuestao != null) {
            tipoQuestaoDAO.excluir(tipoQuestao);
            return Response.noContent().build();
        } else {
            return Response.status(Response.Status.NOT_FOUND).build();
        }
    }
}

