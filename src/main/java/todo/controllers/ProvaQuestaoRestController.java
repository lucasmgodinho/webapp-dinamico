/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package todo.controllers;
import todo.DAO.ProvaQuestaoDAO;
import todo.model.ProvaQuestao;

import java.util.List;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

@Path("/provaQuestao")
public class ProvaQuestaoRestController {

    private ProvaQuestaoDAO provaQuestaoDAO;

    public ProvaQuestaoRestController() {
        provaQuestaoDAO = new ProvaQuestaoDAO();
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<ProvaQuestao> listarProvaQuestao() {
        return provaQuestaoDAO.listarProvaQuestao();
    }

    @GET
    @Path("/{idProva}/{idQuestao}")
    @Produces(MediaType.APPLICATION_JSON)
    public ProvaQuestao getProvaQuestao(@PathParam("idProva") int idProva, @PathParam("idQuestao") int idQuestao) {
        return provaQuestaoDAO.getProvaQuestao(idProva, idQuestao);
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public ProvaQuestao adicionarProvaQuestao(ProvaQuestao provaQuestao) {
        return provaQuestaoDAO.adicionarProvaQuestao(provaQuestao);
    }

    @PUT
    @Path("/{idProva}/{idQuestao}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public ProvaQuestao atualizarProvaQuestao(@PathParam("idProva") int idProva, @PathParam("idQuestao") int idQuestao, ProvaQuestao provaQuestao) {
        return provaQuestaoDAO.atualizarProvaQuestao(idProva, idQuestao, provaQuestao);
    }

    @DELETE
    @Path("/{idProva}/{idQuestao}")
    @Produces(MediaType.APPLICATION_JSON)
    public ProvaQuestao removerProvaQuestao(@PathParam("idProva") int idProva, @PathParam("idQuestao") int idQuestao) {
        return provaQuestaoDAO.removerProvaQuestao(idProva, idQuestao);
    }
}

