/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package todo.controllers;

import todo.DAO.TipoUsuarioDAO;
import todo.model.TipoUsuario;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

@Path("/tipoUsuario")
public class TipoUsuarioRestController {

    private TipoUsuarioDAO tipoUsuarioDAO;

    public TipoUsuarioRestController() {
        tipoUsuarioDAO = new TipoUsuarioDAO();
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<TipoUsuario> listarTiposUsuario() {
        return tipoUsuarioDAO.listarTiposUsuario();
    }

    @GET
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getTipoUsuario(@PathParam("id") int id) {
        TipoUsuario tipoUsuario = tipoUsuarioDAO.getTipoUsuario(id);
        if (tipoUsuario != null) {
            return Response.ok(tipoUsuario).build();
        } else {
            return Response.status(Response.Status.NOT_FOUND).build();
        }
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response adicionarTipoUsuario(TipoUsuario tipoUsuario) {
        tipoUsuarioDAO.salvar(tipoUsuario);
        return Response.status(Response.Status.CREATED).entity(tipoUsuario).build();
    }

    @PUT
    @Path("/{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response atualizarTipoUsuario(@PathParam("id") int id, TipoUsuario tipoUsuarioAtualizado) {
        TipoUsuario tipoUsuario = tipoUsuarioDAO.getTipoUsuario(id);
        if (tipoUsuario != null) {
            tipoUsuario.setIdTipoUsuario(tipoUsuarioAtualizado.getIdTipoUsuario());
            tipoUsuario.setNomeTipoUsuario(tipoUsuarioAtualizado.getNomeTipoUsuario());
            return Response.ok(tipoUsuario).build();
        } else {
            return Response.status(Response.Status.NOT_FOUND).build();
        }
    }

    @DELETE
    @Path("/{id}")
    public Response excluirTipoUsuario(@PathParam("id") int id) {
        TipoUsuario tipoUsuario = tipoUsuarioDAO.getTipoUsuario(id);
        if (tipoUsuario != null) {
            tipoUsuarioDAO.excluir(tipoUsuario);
            return Response.ok().build();
        } else {
            return Response.status(Response.Status.NOT_FOUND).build();
        }
    }
}
