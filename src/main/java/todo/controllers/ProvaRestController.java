/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package todo.controllers;

import javax.ws.rs.*;
import javax.ws.rs.core.*;
import java.util.List;
import todo.DAO.ProvaDAO;
import todo.model.Prova;

@Path("/provas")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class ProvaRestController {

    private ProvaDAO provaDAO;

    public ProvaRestController() {
        provaDAO = new ProvaDAO();
    }

    @GET
    public List<Prova> listarProvas() {
        return provaDAO.listarProvas();
    }

    @GET
    @Path("/{idProva}")
    public Response getProva(@PathParam("idProva") int idProva) {
        Prova prova = provaDAO.getProva(idProva);
        if (prova != null) {
            return Response.ok(prova).build();
        } else {
            return Response.status(Response.Status.NOT_FOUND).build();
        }
    }

    @POST
    public Response salvarProva(Prova prova) {
        provaDAO.salvar(prova);
        return Response.ok().build();
    }

    @PUT
    @Path("/{idProva}")
    public Response atualizarProva(@PathParam("idProva") int idProva, Prova prova) {
        boolean sucesso = provaDAO.atualizar(idProva, prova);
        if (sucesso) {
            return Response.ok().build();
        } else {
            return Response.status(Response.Status.NOT_FOUND).build();
        }
    }

    @DELETE
    @Path("/{idProva}")
    public Response excluirProva(@PathParam("idProva") int idProva) {
        boolean sucesso = provaDAO.excluir(idProva);
        if (sucesso) {
            return Response.ok().build();
        } else {
            return Response.status(Response.Status.NOT_FOUND).build();
        }
    }
}


