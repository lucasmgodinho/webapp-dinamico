/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package todo.controllers;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import todo.model.Usuario;

@WebServlet(urlPatterns = "/login")
public class LoginController extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) 
            throws ServletException, IOException {
        // 1 - Garantimos que a sessão existe.
        HttpSession session = req.getSession(true);
       
        // 2 - Verificamos se o usuário existe.
        String nome = req.getParameter("username");
        String senha = req.getParameter("password");
       
        // 3 - Verificamos se reconhecemos o usuário.
        // Poderia ser um banco de dados, serviço externo, etc...
        if (nome.equals("admin") && senha.equals("admin")) {
            // 4 - Salvamos o login
            Usuario usuario = new Usuario(); 
            session.setAttribute("usuario", usuario);
            RequestDispatcher rd = req.getRequestDispatcher("/index.jsp");
            rd.forward(req,resp);
        } else {
            req.setAttribute("erro", "Usuário ou senha inválidos.");
            RequestDispatcher rd = req.getRequestDispatcher("/login.jsp");
            rd.forward(req,resp);        
        }       
       
    }
}
