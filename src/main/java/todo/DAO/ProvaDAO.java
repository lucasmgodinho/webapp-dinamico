/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package todo.DAO;

import todo.model.Prova;
import java.util.ArrayList;
import java.util.List;

public class ProvaDAO {

    private List<Prova> provas;
    private int nextId;

    public ProvaDAO() {
        provas = new ArrayList<>();
        nextId = 1;
    }

    public List<Prova> listarProvas() {
        return provas;
    }

    public Prova getProva(int idProva) {
        for (Prova prova : provas) {
            if (prova.getIdProva() == idProva) {
                return prova;
            }
        }
        return null;
    }

    public void salvar(Prova prova) {
        prova.setIdProva(nextId);
        provas.add(prova);
        nextId++;
    }

    public boolean atualizar(int idProva, Prova provaAtualizada) {
        for (Prova prova : provas) {
            if (prova.getIdProva() == idProva) {
                prova.setDataProva(provaAtualizada.getDataProva());
                return true;
            }
        }
        return false;
    }

    public boolean excluir(int idProva) {
        for (Prova prova : provas) {
            if (prova.getIdProva() == idProva) {
                provas.remove(prova);
                return true;
            }
        }
        return false;
    }
}
