/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package todo.DAO;
import todo.model.TipoUsuario;
import java.util.ArrayList;
import java.util.List;

public class TipoUsuarioDAO {

    private List<TipoUsuario> tiposUsuario;
    private int proximoId;

    public TipoUsuarioDAO() {
        tiposUsuario = new ArrayList<>();
        proximoId = 1;
    }

    public void salvar(TipoUsuario tipoUsuario) {
        tipoUsuario.setIdTipoUsuario(proximoId++);
        tiposUsuario.add(tipoUsuario);
    }

    public void excluir(TipoUsuario tipoUsuario) {
        tiposUsuario.remove(tipoUsuario);
    }

    public List<TipoUsuario> listarTiposUsuario() {
        return tiposUsuario;
    }

    public TipoUsuario getTipoUsuario(int id) {
        for (TipoUsuario tipoUsuario : tiposUsuario) {
            if (tipoUsuario.getIdTipoUsuario() == id) {
                return tipoUsuario;
            }
        }
        return null;
    }
}
