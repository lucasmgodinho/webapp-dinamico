/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package todo.DAO;

import todo.model.Resultado;
import java.util.ArrayList;
import java.util.List;

public class ResultadoDAO {
    private List<Resultado> resultados;

    public ResultadoDAO() {
        resultados = new ArrayList<>();
    }

    public void salvar(Resultado resultado) {
        resultados.add(resultado);
    }

    public void excluir(Resultado resultado) {
        resultados.remove(resultado);
    }

    public List<Resultado> listarResultados() {
        return resultados;
    }
}


