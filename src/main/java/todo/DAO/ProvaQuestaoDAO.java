/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package todo.DAO;

import java.util.ArrayList;
import java.util.List;
import todo.model.ProvaQuestao;

public class ProvaQuestaoDAO {

    private List<ProvaQuestao> provaQuestaoList;

    public ProvaQuestaoDAO() {
        provaQuestaoList = new ArrayList<>();
    }

    public List<ProvaQuestao> listarProvaQuestao() {
        return provaQuestaoList;
    }

    public ProvaQuestao getProvaQuestao(int idProva, int idQuestao) {
        for (ProvaQuestao provaQuestao : provaQuestaoList) {
            if (provaQuestao.getIdProva() == idProva && provaQuestao.getIdQuestao() == idQuestao) {
                return provaQuestao;
            }
        }
        return null;
    }

    public ProvaQuestao adicionarProvaQuestao(ProvaQuestao provaQuestao) {
        provaQuestaoList.add(provaQuestao);
        return provaQuestao;
    }

    public ProvaQuestao atualizarProvaQuestao(int idProva, int idQuestao, ProvaQuestao provaQuestaoAtualizada) {
        ProvaQuestao provaQuestao = getProvaQuestao(idProva, idQuestao);
        if (provaQuestao != null) {
            provaQuestao.setIdProva(provaQuestaoAtualizada.getIdProva());
            provaQuestao.setIdQuestao(provaQuestaoAtualizada.getIdQuestao());
            return provaQuestao;
        }
        return null;
    }

    public ProvaQuestao removerProvaQuestao(int idProva, int idQuestao) {
        ProvaQuestao provaQuestao = getProvaQuestao(idProva, idQuestao);
        if (provaQuestao != null) {
            provaQuestaoList.remove(provaQuestao);
            return provaQuestao;
        }
        return null;
    }
}


