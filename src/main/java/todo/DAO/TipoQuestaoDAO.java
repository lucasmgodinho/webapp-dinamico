/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package todo.DAO;

import java.util.ArrayList;
import java.util.List;
import todo.model.TipoQuestao;

public class TipoQuestaoDAO {

    private List<TipoQuestao> tiposQuestao;
    private int proximoId;

    public TipoQuestaoDAO() {
        tiposQuestao = new ArrayList<>();
        proximoId = 1;
    }

    public void salvar(TipoQuestao tipoQuestao) {
        tipoQuestao.setIdTipoQuestao(proximoId);
        tiposQuestao.add(tipoQuestao);
        proximoId++;
    }

    public TipoQuestao buscarTipoQuestao(int id) {
        for (TipoQuestao tipoQuestao : tiposQuestao) {
            if (tipoQuestao.getIdTipoQuestao() == id) {
                return tipoQuestao;
            }
        }
        return null;
    }

    public void atualizar(TipoQuestao tipoQuestao) {
    }

    public void excluir(TipoQuestao tipoQuestao) {
        tiposQuestao.remove(tipoQuestao);
    }

    public List<TipoQuestao> listarTiposQuestao() {
        return tiposQuestao;
    }
}

