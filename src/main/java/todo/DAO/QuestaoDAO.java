/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package todo.DAO;

import java.util.ArrayList;
import java.util.List;
import todo.model.Questao;

public class QuestaoDAO {

    private List<Questao> questoes;

    public QuestaoDAO() {
        questoes = new ArrayList<>();
    }

    public List<Questao> listarQuestoes() {
        return questoes;
    }

    public Questao obterQuestaoPorId(int id) {
        for (Questao questao : questoes) {
            if (questao.getIdQuestao() == id) {
                return questao;
            }
        }
        return null;
    }

    public void adicionarQuestao(Questao questao) {
        questoes.add(questao);
    }

    public boolean atualizarQuestao(Questao questao) {
        for (int i = 0; i < questoes.size(); i++) {
            if (questoes.get(i).getIdQuestao() == questao.getIdQuestao()) {
                questoes.set(i, questao);
                return true;
            }
        }
        return false;
    }

    public boolean excluirQuestao(int id) {
        for (Questao questao : questoes) {
            if (questao.getIdQuestao() == id) {
                questoes.remove(questao);
                return true;
            }
        }
        return false;
    }
}


