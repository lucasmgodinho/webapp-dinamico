package todo.model;

import java.util.Objects;

public class Usuario {

    private int idUsuario;
    private String nome;
    private String email;
    private String senha;
    private int idTipoUsuario;

    public Usuario() {
    }
    
    public Usuario(int idUsuario, String nome, String email, String senha, int idTipoUsuario) {
        this.idUsuario = idUsuario;
        this.nome = nome;
        this.email = email;
        this.senha = senha;
        this.idTipoUsuario = idTipoUsuario;
    }

    public int getIDUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(int idUsuario) {
        this.idUsuario = idUsuario;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

    public int getIdTipoUsuario() {
        return idTipoUsuario;
    }

    public void setIdTipoUsuario(int idTipoUsuario) {
        this.idTipoUsuario = idTipoUsuario;
    }

    @Override
    public String toString() {
        return "Usuario{" + "idUsuario=" + idUsuario + ", nome=" + nome + ", email=" + email + ", senha=" + senha + ", idTipoUsuario=" + idTipoUsuario + '}';
    }
    
    
}
