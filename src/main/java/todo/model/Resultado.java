/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package todo.model;

public class Resultado {

    private int idResultado;
    private double valorObtido;
    private int usuario_idUsuario;
    private int prova_idProva;

    public Resultado() {
    }
    
    public Resultado(int idResultado, double valorObtido, int usuario_idUsuario, int prova_idProva) {
        this.idResultado = idResultado;
        this.valorObtido = valorObtido;
        this.usuario_idUsuario = usuario_idUsuario;
        this.prova_idProva = prova_idProva;
    }
    
    public int getIdResultado() {
        return idResultado;
    }

    public void setIdResultado(int idResultado) {
        this.idResultado = idResultado;
    }

    public double getValorObtido() {
        return valorObtido;
    }

    public void setValorObtido(double valorObtido) {
        this.valorObtido = valorObtido;
    }

    public int getUsuario_idUsuario() {
        return usuario_idUsuario;
    }

    public void setUsuario_idUsuario(int usuario_idUsuario) {
        this.usuario_idUsuario = usuario_idUsuario;
    }

    public int getProva_idProva() {
        return prova_idProva;
    }

    public void setProva_idProva(int prova_idProva) {
        this.prova_idProva = prova_idProva;
    }

    @Override
    public String toString() {
        return "Resultado{" + "idResultado=" + idResultado + ", valorObtido=" + valorObtido + ", usuario_idUsuario=" + usuario_idUsuario + ", prova_idProva=" + prova_idProva + '}';
    }


}

