/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package todo.model;

public class ProvaQuestao {

    private int idProva;
    private int idQuestao;

    public ProvaQuestao() {
    }

    public ProvaQuestao(int idProva, int idQuestao) {
        this.idProva = idProva;
        this.idQuestao = idQuestao;
    }

    public int getIdProva() {
        return idProva;
    }

    public void setIdProva(int idProva) {
        this.idProva = idProva;
    }

    public int getIdQuestao() {
        return idQuestao;
    }

    public void setIdQuestao(int idQuestao) {
        this.idQuestao = idQuestao;
    }
}

