/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package todo.model;

public class Questao {

    private int idQuestao;
    private String descricaoQuestao;
    private String alternativaA;
    private String alternativaB;
    private String alternativaC;
    private String alternativaD;
    private String alternativaE;
    private char questaoCorreta;
    private boolean estadoQuestao;
    private int tipoQuestao_idTipoQuestao;

    public int getIdQuestao() {
        return idQuestao;
    }

    public void setIdQuestao(int idQuestao) {
        this.idQuestao = idQuestao;
    }

    public String getDescricaoQuestao() {
        return descricaoQuestao;
    }

    public void setDescricaoQuestao(String descricaoQuestao) {
        this.descricaoQuestao = descricaoQuestao;
    }

    public String getAlternativaA() {
        return alternativaA;
    }

    public void setAlternativaA(String alternativaA) {
        this.alternativaA = alternativaA;
    }

    public String getAlternativaB() {
        return alternativaB;
    }

    public void setAlternativaB(String alternativaB) {
        this.alternativaB = alternativaB;
    }

    public String getAlternativaC() {
        return alternativaC;
    }

    public void setAlternativaC(String alternativaC) {
        this.alternativaC = alternativaC;
    }

    public String getAlternativaD() {
        return alternativaD;
    }

    public void setAlternativaD(String alternativaD) {
        this.alternativaD = alternativaD;
    }

    public String getAlternativaE() {
        return alternativaE;
    }

    public void setAlternativaE(String alternativaE) {
        this.alternativaE = alternativaE;
    }

    public char getQuestaoCorreta() {
        return questaoCorreta;
    }

    public void setQuestaoCorreta(char questaoCorreta) {
        this.questaoCorreta = questaoCorreta;
    }

    public boolean isEstadoQuestao() {
        return estadoQuestao;
    }

    public void setEstadoQuestao(boolean estadoQuestao) {
        this.estadoQuestao = estadoQuestao;
    }

    public int getTipoQuestao_idTipoQuestao() {
        return tipoQuestao_idTipoQuestao;
    }

    public void setTipoQuestao_idTipoQuestao(int tipoQuestao_idTipoQuestao) {
        this.tipoQuestao_idTipoQuestao = tipoQuestao_idTipoQuestao;
    }
}

