<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<!DOCTYPE html>
<html>
<head>
    <title>Resultados</title>
</head>
<body>
    <h1>Resultados</h1>

    <h2>Cadastro</h2>
    <form method="post" action="#{resultadoController.salvarResultado}">
        <input type="number" name="valorObtido" placeholder="Valor Obtido" value="#{resultadoController.resultado.valorObtido}" required/><br/>
        <input type="number" name="usuario_idUsuario" placeholder="ID do Usuário" value="#{resultadoController.resultado.usuario_idUsuario}" required/><br/>
        <input type="number" name="prova_idProva" placeholder="ID da Prova" value="#{resultadoController.resultado.prova_idProva}" required/><br/>
        <input type="submit" value="Salvar"/>
    </form>

    <h2>Listagem</h2>
    <table border="1">
        <thead>
            <tr>
                <th>Valor Obtido</th>
                <th>ID do Usuário</th>
                <th>ID da Prova</th>
                <th>Ações</th>
            </tr>
        </thead>
        <tbody>
            <c:forEach items="#{resultadoController.resultados}" var="resultado">
                <tr>
                    <td><c:out value="#{resultado.valorObtido}"/></td>
                    <td><c:out value="#{resultado.usuario_idUsuario}"/></td>
                    <td><c:out value="#{resultado.prova_idProva}"/></td>
                    <td>
                        <a href="#" onclick="editarResultado(<c:out value="#{resultado.valorObtido}"/>, <c:out value="#{resultado.usuario_idUsuario}"/>, <c:out value="#{resultado.prova_idProva}"/>);">Editar</a>
                        <a href="#" onclick="excluirResultado(<c:out value="#{resultado.valorObtido}"/>, <c:out value="#{resultado.usuario_idUsuario}"/>, <c:out value="#{resultado.prova_idProva}"/>);">Excluir</a>
                    </td>
                </tr>
            </c:forEach>
        </tbody>
    </table>

    <script>
        function editarResultado(valorObtido, usuario_idUsuario, prova_idProva) {
        }

        function excluirResultado(valorObtido, usuario_idUsuario, prova_idProva) {
        }
    </script>
</body>
</html>

