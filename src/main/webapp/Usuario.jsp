

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Usuários</title>
    </head>
    <body>
        <h1>Usuários</h1>


    <h2>Cadastro</h2>
    <form method="post" action="#{usuarioController.salvarUsuario}">
        <input type="text" name="nome" placeholder="Nome" value="#{usuarioController.usuario.nome}" required/><br/>
        <input type="email" name="email" placeholder="Email" value="#{usuarioController.usuario.email}" required/><br/>
        <input type="password" name="senha" placeholder="Senha" value="#{usuarioController.usuario.senha}" required/><br/>
        <input type="number" name="idTipoUsuario" placeholder="ID Tipo Usuário" value="#{usuarioController.usuario.idTipoUsuario}" required/><br/>
        <input type="submit" value="Salvar"/>
    </form>

    <h2>Listagem</h2>
    <table border="1">
        <thead>
            <tr>
                <th>ID</th>
                <th>Nome</th>
                <th>Email</th>
                <th>Ações</th>
            </tr>
        </thead>
        <tbody>
            <c:forEach items="#{usuarioController.usuarios}" var="usuario">
                <tr>
                    <td><c:out value="#{usuario.IDUsuario}"/></td>
                    <td><c:out value="#{usuario.nome}"/></td>
                    <td><c:out value="#{usuario.email}"/></td>
                    <td>
                        <a href="#" onclick="editarUsuario(<c:out value="#{usuario.IDUsuario}"/>);">Editar</a>
                        <a href="#" onclick="excluirUsuario(<c:out value="#{usuario.IDUsuario}"/>);">Excluir</a>
                    </td>
                </tr>
            </c:forEach>
        </tbody>
    </table>

    <script>
        function editarUsuario(IDUsuario) {
        }

        function excluirUsuario(IDUsuario) {
        }
    </script>
</body>
</html>
