<%-- 
    Document   : TipoQuestao
    Created on : 1 de jul. de 2023, 00:25:04
    Author     : lucas
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
    <title>Tipos de Questão</title>
</head>
<body>
    <h1>Tipos de Questão</h1>

    <h2>Cadastro</h2>
    <form method="post" action="#{tipoQuestaoController.salvarTipoQuestao}">
        <input type="number" name="idTipoQuestao" placeholder="ID do Tipo de Questão" value="#{tipoQuestaoController.tipoQuestao.idTipoQuestao}" required/><br/>
        <input type="text" name="nomeTipoQuestao" placeholder="Nome do Tipo de Questão" value="#{tipoQuestaoController.tipoQuestao.nomeTipoQuestao}" required/><br/>
        <input type="submit" value="Salvar"/>
    </form>

    <h2>Listagem</h2>
    <table border="1">
        <thead>
            <tr>
                <th>ID do Tipo de Questão</th>
                <th>Nome</th>
                <th>Ações</th>
            </tr>
        </thead>
        <tbody>
            <c:forEach items="#{tipoQuestaoController.tiposQuestao}" var="tipoQuestao">
                <tr>
                    <td><c:out value="#{tipoQuestao.idTipoQuestao}"/></td>
                    <td><c:out value="#{tipoQuestao.nomeTipoQuestao}"/></td>
                    <td>
                        <a href="#" onclick="editarTipoQuestao(<c:out value="#{tipoQuestao.idTipoQuestao}"/>);">Editar</a>
                        <a href="#" onclick="excluirTipoQuestao(<c:out value="#{tipoQuestao.idTipoQuestao}"/>);">Excluir</a>
                    </td>
                </tr>
            </c:forEach>
        </tbody>
    </table>

    <script>
        function editarTipoQuestao(idTipoQuestao) {
        }

        function excluirTipoQuestao(idTipoQuestao) {
        }
    </script>
</body>
</html>