<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>TO DO Dinâmico - Página Principal</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <style type="text/css">
            .centro {
                margin: 0 auto;
                width: 480px;
            }
        </style>
    </head>
    <body>
        <div class="centro">
            <h1>TO DO Dinâmico - Página Principal</h1>
            <hr />
            <h2>Bem-vindo, ${requestScope.username}!</h2>
            <h3>Lista de Tarefas:</h3>
            <ul>
                <li>Tarefa 1</li>
                <li>Tarefa 2</li>
                <li>Tarefa 3</li>
            </ul>
            <form action="/todo/logout" method="post">
                <input type="submit" name="sair" value="Sair" /> 
            </form>
        </div>        
    </body>
</html>
