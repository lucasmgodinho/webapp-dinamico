
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Provas</title>
    </head>
    <body>
        <h1>Provas</h1>

        <h2>Cadastro</h2>
        <form method="post" action="#{provaController.salvarProva}">
            <input type="date" name="dataProva" placeholder="Data da Prova" value="#{provaController.prova.dataProva}" required/><br/>
            <input type="submit" value="Salvar"/>
        </form>

        <h2>Listagem</h2>
        <table border="1">
            <thead>
                <tr>
                    <th>Data da Prova</th>
                    <th>Ações</th>
                </tr>
            </thead>
            <tbody>
            <c:forEach items="#{provaController.provas}" var="prova">
                <tr>
                    <td><c:out value="#{prova.dataProva}"/></td>
                <td>
                    <a href="#" onclick="editarProva('<c:out value="#{prova.dataProva}"/>');">Editar</a>
                    <a href="#" onclick="excluirProva('<c:out value="#{prova.dataProva}"/>');">Excluir</a>
                </td>
                </tr>
            </c:forEach>
        </tbody>
    </table>

    <script>
        function editarProva(dataProva) {
        }

        function excluirProva(dataProva) {
        }
    </script>
</body>
</html>

