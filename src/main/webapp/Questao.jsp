<%-- 
    Document   : Questao
    Created on : 1 de jul. de 2023, 00:12:01
    Author     : lucas
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
    <title>Questões</title>
</head>
<body>
    <h1>Questões</h1>

    <h2>Cadastro</h2>
    <form method="post" action="#{questaoController.salvarQuestao}">
        <input type="number" name="idQuestao" placeholder="ID da Questão" value="#{questaoController.questao.idQuestao}" required/><br/>
        <input type="text" name="descricaoQuestao" placeholder="Descrição da Questão" value="#{questaoController.questao.descricaoQuestao}" required/><br/>
        <input type="text" name="alternativaA" placeholder="Alternativa A" value="#{questaoController.questao.alternativaA}" required/><br/>
        <input type="text" name="alternativaB" placeholder="Alternativa B" value="#{questaoController.questao.alternativaB}" required/><br/>
        <input type="text" name="alternativaC" placeholder="Alternativa C" value="#{questaoController.questao.alternativaC}" required/><br/>
        <input type="text" name="alternativaD" placeholder="Alternativa D" value="#{questaoController.questao.alternativaD}" required/><br/>
        <input type="text" name="alternativaE" placeholder="Alternativa E" value="#{questaoController.questao.alternativaE}" required/><br/>
        <input type="text" name="questaoCorreta" placeholder="Questão Correta" value="#{questaoController.questao.questaoCorreta}" required/><br/>
        <input type="checkbox" name="estadoQuestao" value="#{questaoController.questao.estadoQuestao}"/> Ativa<br/>
        <input type="number" name="tipoQuestao_idTipoQuestao" placeholder="ID do Tipo da Questão" value="#{questaoController.questao.tipoQuestao_idTipoQuestao}" required/><br/>
        <input type="submit" value="Salvar"/>
    </form>

    <h2>Listagem</h2>
    <table border="1">
        <thead>
            <tr>
                <th>ID da Questão</th>
                <th>Descrição</th>
                <th>Alternativa A</th>
                <th>Alternativa B</th>
                <th>Alternativa C</th>
                <th>Alternativa D</th>
                <th>Alternativa E</th>
                <th>Questão Correta</th>
                <th>Estado</th>
                <th>ID do Tipo</th>
                <th>Ações</th>
            </tr>
        </thead>
        <tbody>
            <c:forEach items="#{questaoController.questoes}" var="questao">
                <tr>
                    <td><c:out value="#{questao.idQuestao}"/></td>
                    <td><c:out value="#{questao.descricaoQuestao}"/></td>
                    <td><c:out value="#{questao.alternativaA}"/></td>
                    <td><c:out value="#{questao.alternativaB}"/></td>
                    <td><c:out value="#{questao.alternativaC}"/></td>
                    <td><c:out value="#{questao.alternativaD}"/></td>
                    <td><c:out value="#{questao.alternativaE}"/></td>
                    <td><c:out value="#{questao.questaoCorreta}"/></td>
                    <td><c:out value="#{questao.estadoQuestao}"/></td>
                    <td><c:out value="#{questao.tipoQuestao_idTipoQuestao}"/></td>
                    <td>
                        <a href="#" onclick="editarQuestao(<c:out value="#{questao.idQuestao}"/>);">Editar</a>
                        <a href="#" onclick="excluirQuestao(<c:out value="#{questao.idQuestao}"/>);">Excluir</a>
                    </td>
                </tr>
            </c:forEach>
        </tbody>
    </table>

    <script>

        function excluirQuestao(idQuestao) {
        }
    </script>
</body>
</html>
